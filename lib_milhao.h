#ifndef LIB_MILHAO_H
#define LIB_MILHAO_H


struct Perguntas
{
	char questao[300];
	char a[200];
	char b[200];
	char c[200];
	char d[200];
	int qC;
};


struct Jogador
{
	char nome[50];
	int saldo;
};





void dificuldade(int diff, struct Perguntas pergunta[]);
// Valida a dificuldade e invoca carregaPerguntas().


void salvaNome(char nomedoPlayer[], FILE *arquivo, int save); 
// Recebe o nome do usuario com sua pontuação e grava em arquivo .txt.


int iniciaJogo(struct Perguntas pergunta[], struct Jogador *jogador, FILE *arquivo); 
// Depois que as perguntas foram carregadas, essa função questiona ao usuário, qual das opções que ela apresenta é a correta. Também dá a possibilidade de desistir.
// O saldo até o momento da desistência estará sendo armazenado 
 
void ranking(FILE *rec);
// Exibe um arquivo .txt com a relação dos jogadores e suas pontuações.


struct Perguntas carregaPerguntas(int *n, struct Perguntas pergunta[]);
// Carrega as perguntas para serem utilizadas pela função iniciaJogo().

#endif // LIBMILHAO_H
