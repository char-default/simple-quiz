#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lib_milhao.h"

void dificuldade(int diff, struct Perguntas pergunta[])
{
	int *p_diff = &diff;
	if(diff > 0 && diff < 4)
	{
		switch(diff)
		{
			case 1:
				printf("Modo facil escolhido!\n");
				carregaPerguntas(p_diff, pergunta);
				break;
			case 2:
				printf("Modo medio escolhido!\n");
				carregaPerguntas(p_diff, pergunta);
				break;
			case 3:
				printf("Modo dificil escolhido!\n");
				carregaPerguntas(p_diff, pergunta);
				break;
			default:
					break;
		}	
	}
}

struct Perguntas carregaPerguntas(int *n, struct Perguntas pergunta[])
{
	if(*n == 1)
	{
			strcpy(pergunta[0].questao, "Em que estado brasileiro nasceu a apresentadora Xuxa?");
			strcpy(pergunta[0].a,"Rio de Janeiro");
			strcpy(pergunta[0].b,"Rio Grande do Sul");
			strcpy(pergunta[0].c,"Santa Catarina");
			strcpy(pergunta[0].d,"Goias");
			pergunta[0].qC = 2;
			
			strcpy(pergunta[1].questao, "Qual e o nome dado ao estado da agua em forma de gelo?");
			strcpy(pergunta[1].a,"Liquido");
			strcpy(pergunta[1].b,"Solido");
			strcpy(pergunta[1].c,"Gasoso");
			strcpy(pergunta[1].d,"Vaporoso");
			pergunta[1].qC = 2;
			

			strcpy(pergunta[2].questao, "Qual era o apelido da cantora Elis Regina?");
			strcpy(pergunta[2].a,"Gauchinha");
			strcpy(pergunta[2].b,"Paulistinha");
			strcpy(pergunta[2].c,"Pimentinha");
			strcpy(pergunta[2].d,"Andorinha");
			pergunta[2].qC = 3;
			


			strcpy(pergunta[3].questao, "Quem e a namorada do Mickey");
			strcpy(pergunta[3].a,"Margarida");
			strcpy(pergunta[3].b,"Minnie");
			strcpy(pergunta[3].c,"A pequena Sereia");
			strcpy(pergunta[3].d,"Olivia Palito");
			pergunta[3].qC = 2;
			

			strcpy(pergunta[4].questao, "Fidel Castro nasceu em que pais?");
			strcpy(pergunta[4].a,"Jamaica");
			strcpy(pergunta[4].b,"Cuba");
			strcpy(pergunta[4].c,"El Salvador");
			strcpy(pergunta[4].d,"Mexico");
			pergunta[4].qC = 2;
			


			strcpy(pergunta[5].questao, "Qual e o personagem do folcore brasileiro que tem uma perna so?");
			strcpy(pergunta[5].a,"Cuca");
			strcpy(pergunta[5].b,"Negrinho do Pastoreio");
			strcpy(pergunta[5].c,"Boitata");
			strcpy(pergunta[5].d,"Saci-Perere");
			pergunta[5].qC = 4;
			



			strcpy(pergunta[6].questao, "Quem proclamou a republica no Brasil em 1889?");
			strcpy(pergunta[6].a,"Duque de Caxias");
			strcpy(pergunta[6].b,"Marechal Rondon");
			strcpy(pergunta[6].c,"Dom Pedro II");
			strcpy(pergunta[6].d,"Marechal Deodoro");
			pergunta[6].qC = 4;
			



			strcpy(pergunta[7].questao, "Quem e o patrono do exercito brasileiro?");
			strcpy(pergunta[7].a,"Marechal Deodoro");
			strcpy(pergunta[7].b,"Barao de Maua");
			strcpy(pergunta[7].c,"Duque de Caxias");
			strcpy(pergunta[7].d,"Marques de Pombal");
			pergunta[7].qC = 3;
			



			strcpy(pergunta[8].questao, "Quem era o apresentador que reprovava os calouros tocando uma buzina?");
			strcpy(pergunta[8].a,"Raul Gil");
			strcpy(pergunta[8].b,"Bolinha");
			strcpy(pergunta[8].c,"Flavio Cavalcanti");
			strcpy(pergunta[8].d,"Chacrinha");
			pergunta[8].qC = 4;
			



			strcpy(pergunta[9].questao, "O que era Frankenstein, de Mary Shelley?");
			strcpy(pergunta[9].a,"Monstro");
			strcpy(pergunta[9].b,"Gorila");
			strcpy(pergunta[9].c,"Principe");
			strcpy(pergunta[9].d,"Sapo");
			pergunta[9].qC = 1;
			
							
	}
	else
		printf("Ainda esta em desenvolvimento...\n");
}	



int iniciaJogo(struct Perguntas pergunta[], struct Jogador *jogador, FILE *arquivo)
{
	int saldo = 0;
	int desistir = 5;
	int resposta = 0;
	
	for(int i = 0; i < 10; i++)
  {
	
		printf("Pergunta:\t[Valendo R$100]:\n");
		puts(pergunta[i].questao);
		printf("\nOpcoes:\n1 - %s\n2 - %s\n3 - %s\n4 - %s\n5 - Para desistir\n", pergunta[i].a,pergunta[i].b,pergunta[i].c,pergunta[i].d);
		printf("Escolha uma das opçoes:\n");
		scanf("%d", &resposta);
		if(resposta == pergunta[i].qC)
		{
			printf("Resposta certa!\n");
			saldo+= 100;	
		}
		else if(resposta == 5)
		{
			printf("Voce optou por desistir!\n");
			break;	
		}
		else
		{
			printf("Resposta errada!\nGame over!\n");
			break;
		}

   }
	 printf("Sua pontuacao: %d\n", saldo);
	 return saldo;
}


	
void salvaNome(char nomedoPlayer[], FILE *arquivo, int save)
{
	
	char d;
	d = '\n';
	char c[2] = ":";	
	char *token1 = (char *)malloc(sizeof(char) * (50));
	char *token2 = (char *)malloc(sizeof(char) * (50));	
	sprintf(token1, "%s", nomedoPlayer);
	sprintf(token2, "%d", save);
	strcat(token1, c);
	strcat(token1, token2);
	arquivo = fopen("ranking.txt", "a+");
	if(!(arquivo == NULL))
	{
		fputc(d, arquivo);
		fwrite(token1, 1, 50, arquivo);
		fclose(arquivo);
	}
	else
		printf("Nao foi possivel salvar o progresso.\n");	
	
	free(token1);
	free(token2);
}

void ranking(FILE *arquivo)
{	
	arquivo = fopen("ranking.txt", "a+");
	char temp[50];
	if(arquivo == NULL)
		printf("Ranking indisponivel no momento!\n");

	while(fgets(temp, 50, arquivo))
	{	
		printf("\n");	
		printf("%s\n", temp);
	}	

	fclose(arquivo);
}

	

