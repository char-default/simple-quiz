#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lib_milhao.h"

int main()
{
	struct Perguntas perguntas[10];
	
	FILE *in;
	FILE *rank;
	struct Jogador player;
	int dif = 0;
	int escolha = 0;
	printf("Bem vindo ao Mini-Show do Milhao[MODO CONSOLE]!\n");
	printf("\n\n1 - Iniciar Jogo\n2 - Ver Ranking\n3 - Finalizar jogo");
	printf("\n\nOpcao: ");
	scanf("%d", &escolha);
	switch(escolha)
	{
		case 1:	
			player.saldo = 0;	
			printf("Iniciando Jogo[...]\n");
			printf("Digite seu nome:\n");
			scanf(" %[^\n]s", player.nome);
			printf("Escolha uma dificuldade:\n1 - Facil\n2 - Medio\n3 - Dificil\n");
			scanf("%d", &dif);
			dificuldade(dif, perguntas);
			int salvar = 0;
			salvar = iniciaJogo(perguntas, &player, rank);
			char playerN[50];
			strcpy(playerN, player.nome);
			salvaNome(playerN, in, salvar);
			break;
		
		case 2:
			ranking(rank);
			break;
		case 3:
			printf("O jogo esta sendo finalizando[...]\n");
			exit(0);
		default:
			printf("Opcao invalida!\n");
			break;
	}
	
	return 0;
}

